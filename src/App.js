import React, { Component } from 'react';
import './App.css';
import Slider from 'react-rangeslider'
import 'react-rangeslider/lib/index.css'
import Progress from 'react-progressbar';
import { Grid, Row, Col } from 'react-flexbox-grid';
import slideConfig from './config/slideConfig.js'

class App extends Component {
  constructor(props) {
     super(props)
     this.state ={
       data: [
         {
           id : 0,
           label : "volumen 0",
           volume: 80
         },{
           id : 1,
           label : "volumen 1",
           volume: 90
         },{
           id : 2,
           label : "volumen 2",
           volume: 50
         }, {
            id : 3,
            label : "volumen 3",
            volume: 30
          },{
            id : 4,
            label : "volumen 4",
            volume: 40
          },{
            id : 5,
            label : "volumen 5",
            volume: 50
          },
          {
            id : 6,
            label : "volumen 6",
            volume: 60
          },{
            id : 7,
            label : "volumen 7",
            volume: 70
          },{
            id : 8,
            label : "volumen 8",
            volume: 80
          }
       ]
     }
     this.handleOnChange = this.handleOnChange.bind(this);
   }
// no consigo pasar de aqui, intento recoger el value, pero no hay manera
   handleOnChange = (event,id,label) => {
     console.log(this.state.data)
     let data = Array.from(this.state.data);
     let newObject = {
       id : id,
       label : label,
       volume: event
     };
     let newData =  Object.assign({}, newObject);

  data[id] =  newData
     this.setState({
      data
    });
   }
   render() {
     return (
       <div>
            <div className="progressBar" >
               {slideConfig.map((data) => {
                 return(
                   <Progress key={data.id} className="progressBar" completed={this.state.data[data.id].volume} />
                 )
               })}
            </div>
            <Grid fluid>
                <Row>
                  {slideConfig.map((slide) => {
                    return(
                      <Col key= {slide.label} xs={1} md={1}  className="slideContain ">
                         <h5 >{slide.label}</h5>
                          <Slider
                              value= {this.state.data[slide.id].volume}
                              orientation="vertical"
                              onChange={ (event) => this.handleOnChange(event,slide.id,slide.label) }
                          />
                      </Col>
                    )
                  })}
                 </Row>
             </Grid>
       </div>
     )
   }
  }


export default App;
