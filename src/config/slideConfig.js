module.exports = [
  {
    id : 0,
    label : "volumen 0",
    volume: 80
  },{
    id : 1,
    label : "volumen 1",
    volume: 90
  },{
    id : 2,
    label : "volumen 2",
    volume: 50
  }, {
     id : 3,
     label : "volumen 3",
     volume: 30
   },{
     id : 4,
     label : "volumen 4",
     volume: 40
   },{
     id : 5,
     label : "volumen 5",
     volume: 50
   },
   {
     id : 6,
     label : "volumen 6",
     volume: 60
   },{
     id : 7,
     label : "volumen 7",
     volume: 70
   },{
     id : 8,
     label : "volumen 8",
     volume: 80
   }
]
